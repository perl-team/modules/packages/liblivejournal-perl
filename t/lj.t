#!/usr/bin/perl -w

# $Id: lj.t,v 1.2 2001/03/15 23:11:31 archon Exp $

BEGIN { $| = 1; print "1..3\n"; }
END {print "not ok 1\n" unless $loaded;}
use LiveJournal;
$loaded = 1;
print "ok 1\n";

# we just want to try login()
my $login = "bogus";
my $pass = "bogus";

print "2..3\n";

my %hash = (
	user => $login,
	password => $pass,
);

my $lj = LiveJournal->new(\%hash);

if ($lj) {
	print "ok 2\n";
	print "3..3\n";
	my $log = $lj->login();
	if ($log) {
		print "ok 3\n";
	} else {
		print "not ok 3\n";
	}
} else {
	print "not ok 2\n";
}
